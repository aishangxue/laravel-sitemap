<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>
<?= '<?xml-stylesheet href="' . route("sitemap.style") . '" type="text/xsl"?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
    @foreach($items as $item)
    <url>
        <loc>{{$item['loc']}}</loc>
    @if(!empty($item['lastmod']))
        @if(\is_string($item['lastmod']))
        <lastmod>{{$item['lastmod']}}</lastmod>
        @else
        <lastmod>{{$item['lastmod']->format(DateTime::ATOM)}}</lastmod>
        @endif
    @endif
        @if(!empty($item['freq']))
        <changefreq>{{$item['freq']}}</changefreq>
        @endif
        @if(!empty($item['priority']))
        <priority>{{$item['priority']}}</priority>
        @endif
    </url>
    @endforeach
</urlset>