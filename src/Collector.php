<?php

namespace SC\Sitemap;

use Closure;


class Collector
{
    protected $items = [];

    public function add($loc, $lastmod=null, $freq=null, $priority=null)
    {
        return $this->addItem([
            'loc' => $loc,
            'lastmod' => $lastmod,
            'freq' => $freq,
            'priority' => $priority,
        ]);
    }

    public function addItem($item)
    {
        $item['loc'] = url($item['loc']);
        $this->items[] = $item;
        return $this;
    }

    public function addCollection($collection, Closure $closure=null)
    {
        if (is_null($closure)) {
            $closure = function($item) {
                return $item->permalink ?: $item->permanent_link;
            };
        }
        foreach($collection as $item) {
            $res = $closure($item);
            if (is_string($res)) {
                $this->add($res);
            } else {
                $this->addItem($res);
            }
        }
    }

    public function items()
    {
        return $this->items;
    }
}