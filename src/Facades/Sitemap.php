<?php

namespace SC\Sitemap\Facades;

use Illuminate\Support\Facades\Facade;


class Sitemap extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \SC\Sitemap\Sitemap::class;
    }
}