<?php

namespace SC\Sitemap\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class InstallCommand extends Command
{
    protected $signature = 'sitemap:install';
    protected $description = 'Install sitemap components';

    public function handle(Filesystem $fs)
    {
        $destPath = app_path('sitemap.php');
        if (!\is_file($destPath)) {
            $fs->copy(__DIR__.'/../../resources/stubs/sitemap.stub', $destPath);
        }
    }
}
