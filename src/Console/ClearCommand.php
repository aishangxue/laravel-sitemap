<?php

namespace SC\Sitemap\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

use SC\Sitemap\Facades\Sitemap;


class ClearCommand extends Command
{
    protected $signature = 'sitemap:clear';
    protected $description = 'Clear sitemap cache';

    public function handle()
    {
        Cache::forget(Sitemap::cacheKey());
        $this->info('clear sitemap successful');
    }
}
