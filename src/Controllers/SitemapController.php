<?php

namespace SC\Sitemap\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SitemapController extends Controller
{
    public function index()
    {
        return response(\Sitemap::render())->header('Content-Type', 'text/xml');
    }

    public function style()
    {
        return response()->view('sitemap::style')->header('Content-Type', 'text/css; charset=utf-8');
    }
}
